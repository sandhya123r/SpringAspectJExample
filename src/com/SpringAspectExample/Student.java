package com.SpringAspectExample;

public class Student {
	private Integer age;
	private String name;
	
	public void setAge(Integer age) {
		this.age = age;
	}
	
	public Integer getAge() {
		return age;
	}

	public String getName() {
	      System.out.println("Name : " + name );
	      return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void printThrowException(){
	      System.out.println("Exception raised");
	      throw new IllegalArgumentException();
	}
	
}
