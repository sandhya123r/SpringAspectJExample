package com.SpringAspectExample;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
	
		ApplicationContext context = new ClassPathXmlApplicationContext("file:src/Beans.xml");
	      

		Student student = (Student) context.getBean("student");
		student.getName();
		
		student.printThrowException();
	}
}
