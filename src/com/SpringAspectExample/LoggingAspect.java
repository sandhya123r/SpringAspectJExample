package com.SpringAspectExample;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;

@Aspect
public class LoggingAspect {
	
	 @Pointcut("execution(* com.SpringAspectExample.*.*(..))")
	   private void selectAll(){}
 
   
	@Before("selectAll()")
	public void beforeAdvice(){
	      System.out.println("Aspect : Going to setup student profile.");
	}
	
	@After("selectAll()")
	public void afterAdvice(){
		System.out.println("Aspect : After student profile setup.");
	}
	
	@AfterReturning("selectAll()")
	public void doAfterReturningTask() {
		System.out.println("Aspect : student get name successfully returned");
	}
	
	@AfterThrowing("selectAll()")
	public void AfterThrowingAdvice() {
		System.out.println("Aspect : exception thrown");
	}
}
